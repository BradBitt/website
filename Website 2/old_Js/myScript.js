function homeInfoOn(){
    var button = document.getElementById('infoButton');

    if (button.innerHTML === '<p id="infoButtonText">I Have used HTML and CSS to design this page, from the social media buttons to the navigation bar that lights up when you hover over it. This page is a blog of my time at University. This includes all major events and activities i have taken part in including extra-curricular activites.</p>'){
        button.innerHTML = '<i id="infoButtonIcon" class="fa fa-info-circle" aria-hidden="true"></i>';
    }
    else {
        button.innerHTML = '<p id="infoButtonText">I Have used HTML and CSS to design this page, from the social media buttons to the navigation bar that lights up when you hover over it. This page is a blog of my time at University. This includes all major events and activities i have taken part in including extra-curricular activites.</p>';
    }
}
function aboutInfoOn(){
    var button = document.getElementById('infoButton');

    if (button.innerHTML === '<p id="infoButtonText">On this page i have displayed a little more about myself. On the desktop version i have merged this page with the home page, and only on mobile you can view the about page.</p>'){
        button.innerHTML = '<i id="infoButtonIcon" class="fa fa-info-circle" aria-hidden="true"></i>';
    }
    else {
        button.innerHTML = '<p id="infoButtonText">On this page i have displayed a little more about myself. On the desktop version i have merged this page with the home page, and only on mobile you can view the about page.</p>';
    }
}
function athleticsInfoOn(){
    var button = document.getElementById('infoButton');

    if (button.innerHTML === '<p id="infoButtonText">I have used HTML to place the item into the table. I have used CSS to design how the table looks such as the borders, and also the lightblue highleight when you hover over a row. Furthermore, i have used Javascript so you can sort the table how you like. e.g Alphabetical order.</p>'){
        button.innerHTML = '<i id="infoButtonIcon" class="fa fa-info-circle" aria-hidden="true"></i>';
    }
    else {
        button.innerHTML = '<p id="infoButtonText">I have used HTML to place the item into the table. I have used CSS to design how the table looks such as the borders, and also the lightblue highleight when you hover over a row. Furthermore, i have used Javascript so you can sort the table how you like. e.g Alphabetical order.</p>';
    }
}
//sorts table for meeting and Venue, only works with alphabetical.
function sortTable(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("myTable");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc"; 
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.getElementsByTagName("TR");
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount ++;      
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}