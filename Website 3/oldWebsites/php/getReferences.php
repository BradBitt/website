<?php
	$readReferences = fopen("php/references.txt","r"); //opens text file
	
	while(false !== ($firstName = fgets($readReferences))){ //checks next string is not empty
		$lastName = fgets($readReferences);
		$occupation = fgets($readReferences);
		$contactInfo = fgets($readReferences);
		$reference = fgets($readReferences);
		echo "<div class='referenceContainer'><h3 class='referenceHeader'>$firstName $lastName</h3><h3 class='referenceHeaderSmall'>occupation: $occupation</h3><h3 class='referenceHeaderSmall'>Contact Info: $contactInfo</h3><div class='referenceTextContainer'><p class='referenceText'>$reference</p></div></div>";
	}
	fclose($readReferences); //closes the file.
?>