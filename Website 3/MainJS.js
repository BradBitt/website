/* Scroll Spy Animation and colour changing */
$(document).ready(function () {
    $('body').scrollspy({
        target: '#siteNavBar'
    })

    // Add smooth scrolling on all links inside the navbar
    $(".experienceLink > a").on('click', function (event) {
        $('html, body').animate({
            scrollTop: $(".experienceContainer").offset().top - 50
        }, 1000);
        return false;
    });
    $(".projectsLink > a").on('click', function (event) {
        $('html, body').animate({
            scrollTop: $(".myProjectContainer").offset().top - 50
        }, 1000);
        return false;
    });
    $(".skillsLink > a").on('click', function (event) {
        $('html, body').animate({
            scrollTop: $(".skillsContainer").offset().top - 50
        }, 1000);
        return false;
    });
    $(".athleticsLink > a").on('click', function (event) {
        $('html, body').animate({
            scrollTop: $(".athleticsContainer").offset().top - 50
        }, 1000);
        return false;
    });
    $(".linksLink > a").on('click', function (event) {
        $('html, body').animate({
            scrollTop: $(".linksContainer").offset().top - 50
        }, 1000);
        return false;
    });
});
/* END of Scroll Spy Animation and colour changing */

/* NavBar Menu Icon animation */
$('.navbar-toggler').on('click', function (event) {
    if ($('.navbar-toggler-icon').css('display') == 'inline-block') {

        $('.navbar-toggler-icon').fadeOut();
        $('.navbar-toggler-cross').delay(500).fadeIn();

    } else {

        $('.navbar-toggler-cross').fadeOut();
        $('.navbar-toggler-icon').delay(500).fadeIn();

    }
});
/* END of navbar menu icon animation */

/* Experience job box animations */
$('.jobBox').on('click', function (event) {
    var slide1 = $(this).children('.jobSlide1Container');
    var slide2 = $(this).children('.jobSlide2Container');
    var imageContainer = $(this).find('.jobImageContainer');
    var scaleSize = 1.1;

    if ($(slide1).css('display') == 'block') {
        $(slide1).fadeOut();
        $(slide2).delay(500).fadeIn();

        $(imageContainer).css({
            '-webkit-transform': 'scale(' + scaleSize + ')',
            '-moz-transform': 'scale(' + scaleSize + ')',
            '-ms-transform': 'scale(' + scaleSize + ')',
            '-o-transform': 'scale(' + scaleSize + ')',
            'transform': 'scale(' + scaleSize + ')'
        });
    } else {
        $(slide2).fadeOut();
        $(slide1).delay(500).fadeIn();

        $(imageContainer).css({
            '-webkit-transform': 'scale(' + 1.0 + ')',
            '-moz-transform': 'scale(' + 1.0 + ')',
            '-ms-transform': 'scale(' + 1.0 + ')',
            '-o-transform': 'scale(' + 1.0 + ')',
            'transform': 'scale(' + 1.0 + ')'
        });
    }
});

$('#jobMoreButton').on('click', function (event) {
    var moreJobSection = $('.moreJobSection');
    var jobMoreButton = $('#jobMoreButton > p');

    if ($(moreJobSection).css('height') == '0px') {
        $(moreJobSection).animate({
            maxHeight: '1200px'
        }, 1000);
        $(jobMoreButton).html("View less");
    } else {
        $(moreJobSection).animate({
            maxHeight: '0px'
        }, 1000);
        $(jobMoreButton).html("View more");
    }
});
/* end */

/* Club container click */
$('.clubBox').on('click', function (event) {
    var slide1 = $(this).children('.clubSlide1Container');
    var slide2 = $(this).children('.clubSlide2Container');
    var imageContainer = $(this).find('.clubImageContainer');
    var scaleSize = 1.1;

    if ($(slide1).css('display') == 'block') {
        $(slide1).fadeOut();
        $(slide2).delay(500).fadeIn();

        $(imageContainer).css({
            '-webkit-transform': 'scale(' + scaleSize + ')',
            '-moz-transform': 'scale(' + scaleSize + ')',
            '-ms-transform': 'scale(' + scaleSize + ')',
            '-o-transform': 'scale(' + scaleSize + ')',
            'transform': 'scale(' + scaleSize + ')'
        });
    } else {
        $(slide2).fadeOut();
        $(slide1).delay(500).fadeIn();

        $(imageContainer).css({
            '-webkit-transform': 'scale(' + 1.0 + ')',
            '-moz-transform': 'scale(' + 1.0 + ')',
            '-ms-transform': 'scale(' + 1.0 + ')',
            '-o-transform': 'scale(' + 1.0 + ')',
            'transform': 'scale(' + 1.0 + ')'
        });
    }
});
/* END */

//Gallery JS
//current image on overlay
var currentImage;

function loadImage(image) {
    imageSrc = image.src;
    currentImage = image.id;

    $('body').append("<div id='imageOverlay' class='imageOverlay'><div class='makeOpacity'></div><div class='overlayRightArrowContainer' onclick='imageRight()'><img src='images/icon_arrow.png'></div><div class='overlayLeftArrowContainer' onclick='imageLeft()'><img src='images/icon_arrow_left.png'></div><div class='imageOverlayCloseBox' onclick='closeImage()'><img src='images/Icon_close.png'></div><img src=" + imageSrc + " id='mainOverlayImage'></div>");
}

function closeImage() {
    $("#imageOverlay").remove();
}

function imageRight() {

    var nextImage = $('#' + currentImage).next().attr('id');
    if (nextImage == null) {
        currentImage = "pic1";
        var newSrc = $('#' + currentImage).attr('src');
        $('#mainOverlayImage').attr("src", newSrc);
    } else {
        var newSrc = $('#' + currentImage).next().attr('src');
        currentImage = $('#' + currentImage).next().attr('id');

        $('#mainOverlayImage').attr("src", newSrc)
    }
}

function imageLeft() {

    var nextImage = $('#' + currentImage).prev().attr('id');
    if (nextImage == null) {
        currentImage = $('#pic1').siblings(":last").attr('id');
        var newSrc = $('#' + currentImage).attr('src');
        $('#mainOverlayImage').attr("src", newSrc);
    } else {
        var newSrc = $('#' + currentImage).prev().attr('src');
        currentImage = $('#' + currentImage).prev().attr('id');

        $('#mainOverlayImage').attr("src", newSrc)
    }
}
/* END */

/* CHNAGE NAVBAR FROM NO COLOUR TO COLOUR */
var distance = $('.experienceContainer').offset().top,
    $window = $(window);
var navBarOuterContainer = $('.navBarOuterContainer');

$(document).ready(function () {
    if ($window.scrollTop() >= distance) {
        $(navBarOuterContainer).css("background-color", "#f2f2f2");
    } else {
        $(navBarOuterContainer).css("background-color", "transparent");
    }
});

$window.scroll(function () {
    if ($window.scrollTop() >= distance) {
        $(navBarOuterContainer).css("background-color", "#f2f2f2");
    } else {
        $(navBarOuterContainer).css("background-color", "transparent");
    }
});
