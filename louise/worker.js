// This is the worker thread.
// Everything done here is a seperate thread to the main thread.
this.onmessage = function(e) {
    if(e.data.date !== undefined) {
        
        var dateNew = new Date();
        var timeDiffNew = Math.abs(dateNew.getTime() - e.data.date.date1.getTime());
        this.postMessage( {result: timeDiffNew} );
    }
}