//when the page is first loaded all these functions are called.
function onLoadFunctions(){
	identifyPage();
}

//removes the did you know box
function closeBox(){
	var elem = document.getElementById('didYouKnowBox');
	elem.parentNode.removeChild(elem);
}

//chnages color of star when hover over it
function highlight1(){
	document.getElementById("star1").style.color = "yellow";
	document.getElementById('starTextBox').innerHTML = '<p id="starTextBox">1 Star</p>'; 
}
//changes color of star when not hovering over it.
function highlight1Off(){
	document.getElementById("star1").style.color = "grey";
	document.getElementById('starTextBox').innerHTML = '<p id="starTextBox"></p>'; 
}
function highlight2(){
	document.getElementById("star1").style.color = "yellow";
	document.getElementById("star2").style.color = "yellow";
	document.getElementById('starTextBox').innerHTML = '<p id="starTextBox">2 Star</p>'; 
}
function highlight2Off(){
	document.getElementById("star1").style.color = "grey";
	document.getElementById("star2").style.color = "grey";
	document.getElementById('starTextBox').innerHTML = '<p id="starTextBox"></p>'; 
}
function highlight3(){
	document.getElementById("star1").style.color = "yellow";
	document.getElementById("star2").style.color = "yellow";
	document.getElementById("star3").style.color = "yellow";
	document.getElementById('starTextBox').innerHTML = '<p id="starTextBox">3 Star</p>'; 
}
function highlight3Off(){
	document.getElementById("star1").style.color = "grey";
	document.getElementById("star2").style.color = "grey";
	document.getElementById("star3").style.color = "grey";
	document.getElementById('starTextBox').innerHTML = '<p id="starTextBox"></p>'; 
}
function highlight4(){
	document.getElementById("star1").style.color = "yellow";
	document.getElementById("star2").style.color = "yellow";
	document.getElementById("star3").style.color = "yellow";
	document.getElementById("star4").style.color = "yellow";
	document.getElementById('starTextBox').innerHTML = '<p id="starTextBox">4 Star</p>'; 
}
function highlight4Off(){
	document.getElementById("star1").style.color = "grey";
	document.getElementById("star2").style.color = "grey";
	document.getElementById("star3").style.color = "grey";
	document.getElementById("star4").style.color = "grey";
	document.getElementById('starTextBox').innerHTML = '<p id="starTextBox"></p>'; 
}
function highlight5(){
	document.getElementById("star1").style.color = "yellow";
	document.getElementById("star2").style.color = "yellow";
	document.getElementById("star3").style.color = "yellow";
	document.getElementById("star4").style.color = "yellow";
	document.getElementById("star5").style.color = "yellow";
	document.getElementById('starTextBox').innerHTML = '<p id="starTextBox">5 Star</p>'; 
}
function highlight5Off(){
	document.getElementById("star1").style.color = "grey";
	document.getElementById("star2").style.color = "grey";
	document.getElementById("star3").style.color = "grey";
	document.getElementById("star4").style.color = "grey";
	document.getElementById("star5").style.color = "grey";
	document.getElementById('starTextBox').innerHTML = '<p id="starTextBox"></p>'; 
}

/* javascript function that takes a rating the user has given, sends it to the server 
and stores it in a text document. it then updates the total score of rating and 
returns the new total and the new total score is then displayed.
*/
function saveRating(num){
	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			//The stars changed and attributes removed and a thank you message underneath appears.
			document.getElementById('starTextBox').innerHTML = '<p id="starTextBox">Thank You!</p>'; 
			
			if(num == 1){
				document.getElementById('star1').innerHTML = '<li id="starDoneYellow" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star2').innerHTML = '<li id="starDoneGrey" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star3').innerHTML = '<li id="starDoneGrey" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star4').innerHTML = '<li id="starDoneGrey" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star5').innerHTML = '<li id="starDoneGrey" ><i class="fa fa-star" aria-hidden="true"></i></li>';
			}else if(num == 2){
				document.getElementById('star1').innerHTML = '<li id="starDoneYellow" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star2').innerHTML = '<li id="starDoneYellow" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star3').innerHTML = '<li id="starDoneGrey" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star4').innerHTML = '<li id="starDoneGrey" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star5').innerHTML = '<li id="starDoneGrey" ><i class="fa fa-star" aria-hidden="true"></i></li>';
			}else if(num == 3){
				document.getElementById('star1').innerHTML = '<li id="starDoneYellow" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star2').innerHTML = '<li id="starDoneYellow" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star3').innerHTML = '<li id="starDoneYellow" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star4').innerHTML = '<li id="starDoneGrey" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star5').innerHTML = '<li id="starDoneGrey" ><i class="fa fa-star" aria-hidden="true"></i></li>';
			}else if(num == 4){
				document.getElementById('star1').innerHTML = '<li id="starDoneYellow" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star2').innerHTML = '<li id="starDoneYellow" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star3').innerHTML = '<li id="starDoneYellow" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star4').innerHTML = '<li id="starDoneYellow" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star5').innerHTML = '<li id="starDoneGrey" ><i class="fa fa-star" aria-hidden="true"></i></li>';
			}else if(num == 5){
				document.getElementById('star1').innerHTML = '<li id="starDoneYellow" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star2').innerHTML = '<li id="starDoneYellow" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star3').innerHTML = '<li id="starDoneYellow" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star4').innerHTML = '<li id="starDoneYellow" ><i class="fa fa-star" aria-hidden="true"></i></li>';
				document.getElementById('star5').innerHTML = '<li id="starDoneYellow" ><i class="fa fa-star" aria-hidden="true"></i></li>';
			}
			document.getElementById('star1').removeAttribute("onmouseover");
			document.getElementById('star1').removeAttribute("onmouseout");
			document.getElementById('star1').removeAttribute("onclick");
			document.getElementById('star2').removeAttribute("onmouseover");
			document.getElementById('star2').removeAttribute("onmouseout");
			document.getElementById('star2').removeAttribute("onclick");
			document.getElementById('star3').removeAttribute("onmouseover");
			document.getElementById('star3').removeAttribute("onmouseout");
			document.getElementById('star3').removeAttribute("onclick");
			document.getElementById('star4').removeAttribute("onmouseover");
			document.getElementById('star4').removeAttribute("onmouseout");
			document.getElementById('star4').removeAttribute("onclick");
			document.getElementById('star5').removeAttribute("onmouseover");
			document.getElementById('star5').removeAttribute("onmouseout");
			document.getElementById('star5').removeAttribute("onclick");
		}
	};
	xmlhttp.open("GET", "php/storeRating.php?x=" + num, true);
	xmlhttp.send();
}

//identifies what page the person is on
function identifyPage(){
	if(document.getElementById('pageTitle').innerHTML == "Home"){
		document.getElementById("listItemHome").style.borderBottom = "4px solid white";
	}else if(document.getElementById('pageTitle').innerHTML == "About"){
		document.getElementById("listItemAbout").style.borderBottom = "4px solid white";
	}else if(document.getElementById('pageTitle').innerHTML == "Athletics"){
		document.getElementById("listItemAthletics").style.borderBottom = "4px solid white";
	}else if(document.getElementById('pageTitle').innerHTML == "Projects"){
		document.getElementById("listItemProjects").style.borderBottom = "4px solid white";
	}else if(document.getElementById('pageTitle').innerHTML == "References"){
		document.getElementById("listItemReferences").style.borderBottom = "4px solid white";
	}
}

//sorts table for meeting and Venue, only works with alphabetical.
function sortTable(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("myTable");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc"; 
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.getElementsByTagName("TR");
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount ++;      
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

/*
A function which extends the size of a div so all the content is shown, it allows you 
to then minimize the div and return it to its original size.
*/
function expandProject() {
	var inner = document.getElementById('extendProjectText').innerHTML;
	if(inner == 'Extend'){
		document.getElementById('projectsContainer').style.height = 'auto';
		document.getElementById('extendProjectText').innerHTML = 'Minimize';
	}else if(inner != 'Extend'){
		document.getElementById('projectsContainer').style.height = '200px';
		document.getElementById('extendProjectText').innerHTML = 'Extend';
	}
}

/*
A function that allows you to click the reference navbar icon to open the drop down menu
and have it remain there rather than just apear on hover. This is useful for mobile.
*/
function toggleMenu(){
	var menuStatus = document.getElementById("dropDownMenu").style.display;
	if(menuStatus == 'block'){
		document.getElementById("dropDownMenu").style.display = 'none';
	}else if(menuStatus != 'block'){
		document.getElementById("dropDownMenu").style.display = 'block';
	}
}