<?php
	/* Collects all the numbers stored in the file, it then calculates the 
	average and then returns this number.
	*/
	
	//declares a total of 0 first.
	$total = 0;
	
	//declares a count.
	$count = 0;
	//opens a file to be read.
	$readNumFile = fopen("javaScript/php/ratingNumbers.txt","r"); //read file.
	
	while(false !== ($char = fgetc($readNumFile))){
	$total += $char; 
	$count += 1;
	}
	fclose($readNumFile); //closes the file.
	
	//calculates the average rating.
	$ratingResult = $total / $count;
	
	//returns the rating result as well as has 1 decimal place.
	echo number_format((float)$ratingResult, 1, '.', ' ');
?>
