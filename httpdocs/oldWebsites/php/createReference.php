<?php
	$firstName = $_POST['firstName'];
	$lastName = $_POST['lastName'];
	$occupation = $_POST['occupation'];
	$contactInfo = $_POST['contactInfo'];
	$reference = $_POST['reference'];
	
	
	//opens text file and sets to appending the file.
	$myfile = fopen("references.txt", "a");
	
	fwrite($myfile, $firstName);
	fwrite($myfile, "\n");
	fwrite($myfile, $lastName);
	fwrite($myfile, "\n");
	fwrite($myfile, $occupation);
	fwrite($myfile, "\n");
	fwrite($myfile, $contactInfo);
	fwrite($myfile, "\n");
	fwrite($myfile, $reference);
	fwrite($myfile, "\n");
	
	//closes the file
	fclose($myfile);
	
	//takes the user back to the previous page.
    header('Location: ' . $_SERVER['HTTP_REFERER']);
?>