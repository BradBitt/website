function start() {
    $('.StartButton').css("display", "none");
    $('.videoContainer').fadeIn();
}

$( document ).ready(function() {
    myMap();
    setTime();
    runTime();
});

// Adds a seperate thread which will have seconds and minutes tick forever.
function runTime() {
    //1438642800000 milliseconds when we got together. (4th August 2015)
    var dateGotTogether = new Date(1438642800000);
    
    // This creates a seperate process that runs alongside the main process.
    if (window.Worker) {
        var myWorker = new Worker("worker.js");
        var message = { date: {date1:dateGotTogether} };
        
        // This sends off the data to be computed in a seperate thread.
        myWorker.postMessage(message);
        
        // This is what is returned and everything in here is also done in a
        // seperate thread. I have used recursion to get it to loop forever.
        // Can't update the DOM inside the worker thread ("worker.js").
        // Editing DOM has to be done in the onmessage function below.
        myWorker.onmessage = function(e) {
            var diffSecsNew = Math.ceil(e.data.result / (1000));
            $('#secTogether').html(diffSecsNew);
            var diffMinsNew = Math.ceil(e.data.result / (1000 * 60));
            $('#minTogether').html(diffMinsNew);
            setTimeout(function(){ 
                myWorker.postMessage(message); 
            }, 1000);
        }
    }
}

// adds times to the webpage.
function setTime(){
    // Date now
    var dateNow = new Date();
    //1438642800000 milliseconds when we got together. (4th August 2015)
    var dateGotTogether = new Date(1438642800000);
    //1425686400000 milliseconds when we first kiss (7th March 2015)
    var dateFirstKiss = new Date(1425686400000);
    //967762800000 milliseconds when we first met (1st January 2000)
    var dateFirstMet = new Date(967762800000);
    
    
    // Time difference for day got together
    var timeDiff = Math.abs(dateNow.getTime() - dateGotTogether.getTime());
    // Time differnce for first kiss
    var timeDiffKiss = Math.abs(dateNow.getTime() - dateFirstKiss.getTime());
    // Time difference for first met
    var timeDiffMet = Math.abs(dateNow.getTime() - dateFirstMet.getTime());
    
    
    // Gets the time difference in days
    var diffDays = Math.ceil(timeDiff / (1000 * 60 * 60 * 24));
    $('#daysTogether').html(diffDays);
    
    // Gets the time difference in hours
    var diffHrs = Math.ceil(timeDiff / (1000 * 60 * 60));
    $('#hrTogether').html(diffHrs);
    
    // Gets the time difference for first kiss
    var diffDaysKiss = Math.ceil(timeDiffKiss / (1000 * 60 * 60 * 24));
    $('#daysSinceKiss').html(diffDaysKiss);
    
    // Gets the time difference for when we first met
    var diffYearsMet = Math.ceil(timeDiffMet / (1000 * 60 * 60 * 24 * 365));
    $('#yearsKnown').html(diffYearsMet);
}

// Sets the locations of places we have been onto the google map.
function myMap() {
    var mapProp = {
        center: new google.maps.LatLng(54.6444519, -4.6828848),
        zoom: 5,
        gestureHandling: "greedy"
    };
    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

    //adds a marker to the map based on co-ordinates.
    // Cardiff
    var myCenter = new google.maps.LatLng(51.4767184, -3.1726365);
    var marker = new google.maps.Marker({
        position: myCenter
    });
    marker.setMap(map);
    var infowindow = new google.maps.InfoWindow({
        content: "Where we spent our first night together."
    });
    marker.addListener('click', function() {
        infowindow.open(marker.get('map'), marker);
    });
    

    // Swansea University
    var myCenter1 = new google.maps.LatLng(51.6184535, -3.9443921);
    var marker1 = new google.maps.Marker({
        position: myCenter1
    });
    marker1.setMap(map);
    var infowindow1 = new google.maps.InfoWindow({
        content: "We went Swansea University together."
    });
    marker1.addListener('click', function() {
        infowindow1.open(marker1.get('map'), marker1);
    });
    

    // Blackpool
    var myCenter2 = new google.maps.LatLng(53.8127059, -3.0406602);
    var marker2 = new google.maps.Marker({
        position: myCenter2
    });
    marker2.setMap(map);
    var infowindow2 = new google.maps.InfoWindow({
        content: "We went Blackpool together for our 1st anniversary."
    });
    marker2.addListener('click', function() {
        infowindow2.open(marker2.get('map'), marker2);
    });
    

    // York
    var myCenter3 = new google.maps.LatLng(53.9431202, -1.0994758);
    var marker3 = new google.maps.Marker({
        position: myCenter3
    });
    marker3.setMap(map);
    var infowindow3 = new google.maps.InfoWindow({
        content: "Came to visit me in York, during my year placement."
    });
    marker3.addListener('click', function() {
        infowindow3.open(marker3.get('map'), marker3);
    });
    
    
    // London
    var myCenter4 = new google.maps.LatLng(51.4864167, -0.1378937);
    var marker4 = new google.maps.Marker({
        position: myCenter4
    });
    marker4.setMap(map);
    var infowindow4 = new google.maps.InfoWindow({
        content: "We have travelled to london together loads of times."
    });
    marker4.addListener('click', function() {
        infowindow4.open(marker4.get('map'), marker4);
    });
    
    
    // Reading
    var myCenter5 = new google.maps.LatLng(51.4444036, -0.9780737);
    var marker5 = new google.maps.Marker({
        position: myCenter5
    });
    marker5.setMap(map);
    var infowindow5 = new google.maps.InfoWindow({
        content: "Went here for dinner and shopping, 2nd anniversary."
    });
    marker5.addListener('click', function() {
        infowindow5.open(marker5.get('map'), marker5);
    });
    

    // Surry University
    var myCenter6 = new google.maps.LatLng(51.2406256, -0.5907693);
    var marker6 = new google.maps.Marker({
        position: myCenter6
    });
    marker6.setMap(map);
    var infowindow6 = new google.maps.InfoWindow({
        content: "Went here summer school together, before we started dating."
    });
    marker6.addListener('click', function() {
        infowindow6.open(marker6.get('map'), marker6);
    });

    
    // Bognor Regis
    var myCenter7 = new google.maps.LatLng(50.778046, -0.6771528);
    var marker7 = new google.maps.Marker({
        position: myCenter7
    });
    marker7.setMap(map);
    var infowindow7 = new google.maps.InfoWindow({
        content: "We have been here a few years for sea side and crazy golf."
    });
    marker7.addListener('click', function() {
        infowindow7.open(marker7.get('map'), marker7);
    });
    
    
    // DisneyLand Paris
    var myCenter8 = new google.maps.LatLng(48.8717479, 2.7756223);
    var marker8 = new google.maps.Marker({
        position: myCenter8
    });
    marker8.setMap(map);
    var infowindow8 = new google.maps.InfoWindow({
        content: "Our first christmas, we went to disneyland Paris."
    });
    marker8.addListener('click', function() {
        infowindow8.open(marker8.get('map'), marker8);
    });
    
    
    // Krakov
    var myCenter9 = new google.maps.LatLng(50.0317916, 19.9174383);
    var marker9 = new google.maps.Marker({
        position: myCenter9
    });
    marker9.setMap(map);
    var infowindow9 = new google.maps.InfoWindow({
        content: "We went on holiday here with our friends."
    });
    marker9.addListener('click', function() {
        infowindow9.open(marker9.get('map'), marker9);
    });
    
    
    // Alton
    var myCenter10 = new google.maps.LatLng(51.146861, -0.9762121);
    var marker10 = new google.maps.Marker({
        position: myCenter10
    });
    marker10.setMap(map);
    var infowindow10 = new google.maps.InfoWindow({
        content: "Went to a bus rally with Rodney."
    });
    marker10.addListener('click', function() {
        infowindow10.open(marker10.get('map'), marker10);
    });
    
    
    // Bridgend
    var myCenter11 = new google.maps.LatLng(51.4990159, -3.5838933);
    var marker11 = new google.maps.Marker({
        position: myCenter11
    });
    marker11.setMap(map);
    var infowindow11 = new google.maps.InfoWindow({
        content: "Went shopping here together."
    });
    marker11.addListener('click', function() {
        infowindow11.open(marker11.get('map'), marker11);
    });
    
    
    // New Forest
    var myCenter12 = new google.maps.LatLng(50.8769321, -1.6375263);
    var marker12 = new google.maps.Marker({
        position: myCenter12
    });
    marker12.setMap(map);
    var infowindow12 = new google.maps.InfoWindow({
        content: "We completed our DofE Silver here together."
    });
    marker12.addListener('click', function() {
        infowindow12.open(marker12.get('map'), marker12);
    });
    
    
    // Slough
    var myCenter13 = new google.maps.LatLng(51.5098994, -0.5938078);
    var marker13 = new google.maps.Marker({
        position: myCenter13
    });
    marker13.setMap(map);
    var infowindow13 = new google.maps.InfoWindow({
        content: "Where we have grown up together."
    });
    marker13.addListener('click', function() {
        infowindow13.open(marker13.get('map'), marker13);
    });
    
    
    // Uxbrdige
    var myCenter14 = new google.maps.LatLng(51.546424, -0.4794191);
    var marker14 = new google.maps.Marker({
        position: myCenter14
    });
    marker14.setMap(map);
    var infowindow14 = new google.maps.InfoWindow({
        content: "We go here for shopping and Cinema together."
    });
    marker14.addListener('click', function() {
        infowindow14.open(marker14.get('map'), marker14);
    });
}
